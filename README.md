# README #

This repository will show how to create a web component from an existing component. 
This example will use the date picker from Vaadin (https://vaadin.com/components).
Will also use the card component from SAP UI5 (https://sap.github.io/ui5-webcomponents/playground/components/CheckBox/)

### What is needed ###
* NPM for downloading the components
* Snowpack https://www.snowpack.dev/#installing-snowpack for converting dependencies or components to a file which is ES6 compatible.
* Check out http://webcomponents-with-redux.training/ for example on how to do it.


### Steps for extracting a Redux to a single file ###
* In your project create a directory with the name ```dependencies```. This directory will hold all the downloading and converting. Suggest creating this file outside of git-project.
* Inside ```dependencies```run ```npm init -y```
* Run ```npm install --save-dev snowpack@next``` to install snowpack. (when snowpack 2 has final release we can skip @next)
* Run ```npm i redux```
* Open package.json and add
```js
"snowpack": {
	"knownEntrypoints": [
		"redux"
	]
}
```
* Run ```npx snowpack``` to create a ES6 module out of redux.


### Things to remeber when copying files generated by snowpack to the project ###
* Create a directory named ```lib```and paste in the ```*.js``` file generated by snowpack. You can name the directory anything you like. But ```lib```made sense.
* In the index.html remeber when importing ```app.js```to active module-system. ```<script src="app.js" type="module"></script>```
* To use the file just import the function inside the file that is needed. ```import { createStore } from './lib/redux.js';```


----------------------------------------------------------------------------------

### The information below is outdated and replaced with snowpack. Less code :) ###

### What is needed ###

* NPM for downloading the components
* Rollup.js for converting the whole project into a single file that includes everything needed for one component.
* Rollup and it's dependency will be installed in the same directory as the web components is installed.

### Steps for extracting CheckBox-component from SAP UI5 ###

1. Make a temporary directory for downloading the component.
2. Install rollup and rollup-plugin-node-resolve. ```npm install rollup rollup-plugin-node-resolve```
3. Inside the directory run the following command ```npm install @ui5/webcomponents``` (The command is fetch from here https://sap.github.io/ui5-webcomponents/playground/)
4. Create a file with name ```rollup.config.js``` inside the same temporary directory.
5. Add the script at the bottom inside the file
6. Run the following for creating the CheckBox.js which we can use in our project. ```npx rollup -c rullup.config.js```
7. This should have created a directory dist which includes a file named "CheckBox.js"
8. Copy this file into the project which needs this module.

## rollup.config.js for SAP U15 Checkbox

```js
import resolve from 'rollup-plugin-node-resolve'; 
export default { 
	input: 'node_modules/@ui5/webcomponents/dist/CheckBox.js', 
	output: { 
		file: './dist/CheckBox.js',
		format: 'esm', 
		name: 'ui5' 
	}, 
	plugins: [ 
		resolve(
			{ 
				mainFields: ['jsnext:main'] 
			}) 
		] 
	}
```

## rollup.config.js for Vaadin date-picker

```js
import resolve from 'rollup-plugin-node-resolve'; 
export default { 
	input: 'node_modules/@vaadin/vaadin-date-picker/vaadin-date-picker.js', 
	output: { 
		file: './dist/vaadin-date-picker.js',
		format: 'esm', 
		name: 'vaadin' 
	}, 
	plugins: [ 
		resolve(
			{ 
				mainFields: ['jsnext:main'] 
			}) 
		] 
	}
```

### Quick breakdown of the rollup.config.js:
- input: This is the location of the component we want to use
- output.file: Output location
- output.format: This is set to ECMAScript Module
- output.name: Global variable name representing the created bundle. Usally name of the vendor.
